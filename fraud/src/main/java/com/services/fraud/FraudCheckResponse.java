package com.services.fraud;

public record FraudCheckResponse(Boolean isFraudster) {
}
