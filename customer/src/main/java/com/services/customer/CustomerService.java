package com.services.customer;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.persistence.Access;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final RestTemplate restTemplate;


    public String getAll(){
        return new Gson().toJson(customerRepository.findAll());
    }


    public void registerCustomer(CustomerRegistrationRequest request){
        //like a constructor
        Customer customer = Customer.builder()
                .firstName(request.firstName())
                .lastName(request.lastName())
                .email(request.email())
                .build();
        //todo:check if email valid
        //todo:check if email not taken
        customerRepository.saveAndFlush(customer);//to get the id of customer here
        //todo:check if fraudster
        FraudCheckResponse fraudCheckResponse= restTemplate.getForObject(
                "http://FRAUD/api/v1/fraud-check/{customerId}",
                FraudCheckResponse.class,
                customer.getId()//here
        );
        if(fraudCheckResponse.isFraudster()){
            throw new IllegalStateException("fraudster");
        }
        //store customer in db
        //todo : send notification

    }
}

