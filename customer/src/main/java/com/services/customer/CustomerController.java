package com.services.customer;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@Slf4j//to log couple things
@RestController
@AllArgsConstructor
@RequestMapping("api/v1/customers")
@CrossOrigin("*")
public class CustomerController {

    private final CustomerService customerService;
    private final CustomerRepository cRepo;

    @GetMapping("/all")
    public String getAll(){
        return customerService.getAll();

    }

    @GetMapping("/all/{id}")
    public Customer getEmployeeById(@PathVariable Integer id){
        return cRepo.findById(id).get();
    }

    @PostMapping
    public void registerCustomer(@RequestBody CustomerRegistrationRequest customerRegistrationRequest){
        log.info("new customer registration {}",customerRegistrationRequest);
        customerService.registerCustomer(customerRegistrationRequest);
    }

    @DeleteMapping("all/{id}")
    public void deleteEmployeeById(@PathVariable Integer id){
        cRepo.deleteById(id);
    }

    @PutMapping("all")
    public Customer updateCustomer(@RequestBody Customer customer) {
        return cRepo.save(customer);
    }


}
